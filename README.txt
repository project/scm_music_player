INTRODUCTION
------------

SCM Music Player is a free and open source web music player, that brings 
a Steady playback music experience to your drupal site.

Continuous Playback Cross Pages - Steady playback throughout your website.
Full Featured Control - Play, pause, next, previous, seek, shuffle, 
repeat mode, volume and more.
Custom Skins - Match your look and feel. Choose or design your own skin 
with CSS.
Dynamic Playlist - Music from various sources: MP3, SoundCloud, Youtube
, RSS in HTML5 or Flash.
Flexible UI - Dockable player on top or bottom. Playlist can be toggled.


REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://www.drupal.org/project/libraries)

This module requires the following library:

 * SCM Music Player (https://github.com/cshum/scm-music-player/archive/github.zip)


INSTALLATION
------------

1. Download and unpack the Libraries module directory in your modules folder
   (this will usually be "sites/all/modules/").
   Link: http://drupal.org/project/libraries
   
2. Download and unpack the "scm_music_player" module directory in your 
   modules folder (this will usually be "sites/all/modules/").
   
3. Download and unpack the SCM Music Player library 
   (https://github.com/cshum/scm-music-player/archive/github.zip) 
   in "sites/all/libraries".
   Make sure the path to the library file becomes:
   "sites/all/libraries/scm-music-player/script.js"
   
4. Go to "Administer" -> "Modules" and enable the "SCM Music Player"
   module.


CONFIGURATION
-------------

* Configure SCM music player in  Administration » Configuration » Media 
» SCM Music Player (admin/config/media/scm_music_player)


TROUBLESHOOTING
---------------
* If you ever have problems, make sure to go through these steps:

- Ensure that the page isn't being served from your browser's cache.
     Use CTRL+R in Windows/Linux browsers, CMD+R in Mac OS X browsers to enforce
     the browser to reload everything, preventing it from using its cache.
	 
- If you ever have problems applied new settings please refresh/reloadthe site 
     to see the changes.
	 
MAINTAINERS
-----------

Current maintainers:
 * Solomon Abraham (abrahamso) - https://www.drupal.org/user/584918