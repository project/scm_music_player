<?php

/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 */

/**
 * Implements hook_field_prepare_view().
 */
function scm_music_player_field_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      if (!file_field_displayed($item, $field)) {
        unset($items[$id][$delta]);
      }
      // Ensure consecutive deltas.
      $items[$id] = array_values($items[$id]);
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function scm_music_player_field_field_is_empty($item, $field) {
  return file_field_is_empty($item, $field);
}

/**
 * Implements hook_field_widget_info().
 */
function scm_music_player_field_field_widget_info() {
  return array(
    'scm_music_player_field_widget' => array(
      'label' => t('scm_music_player Player Upload'),
      'field types' => array(
        'file',
      ),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'file_extensions' => 'mp3,wma',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function scm_music_player_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Add display_field setting to field because file_field_widget_form() assumes
  // it is set.
  $field['settings']['display_field'] = 0;
  $elements = file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $settings = $instance['settings'];
  foreach (element_children($elements) as $delta) {
    $elements[$delta]['#process'] = array(
      'scm_music_player_field_field_widget_process',
    );
  }
  if ($field['cardinality'] == 1) {
    // If there's only one field, return it as delta 0.
    if (empty($elements[0]['#default_value']['fid'])) {
      $elements[0]['#description'] = theme('file_upload_help', array(
        'description' => $instance['description'],
        'upload_validators' => $elements[0]['#upload_validators'],
      ));
    }
  }
  else {
    $elements['#file_upload_description'] = theme('file_upload_help', array(
      'upload_validators' => $elements[0]['#upload_validators'],
    ));
  }
  return $elements;
}

/**
 * Implements hook_field_field_widget_process().
 */
function scm_music_player_field_field_widget_process($element, &$form_state, $form) {
  $fid = isset($element['#value']['fid']) ? $element['#value']['fid'] : 0;
  $element = file_managed_file_process($element, $form_state, $form);
  $item = $element['#value'];
  $item['fid'] = $element['fid']['#value'];
  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);
  // Add the display field if enabled.
  if (!empty($field['settings']['display_field']) && $item['fid']) {
    $element['display'] = array(
      '#type' => empty($item['fid']) ? 'hidden' : 'checkbox',
      '#title' => t('Include file in display'),
      '#value' => isset($item['display']) ? $item['display'] : $field['settings']['display_default'],
      '#attributes' => array(
        'class' => array(
          'file-display',
        ),
      ),
    );
  }
  else {
    $element['display'] = array(
      '#type' => 'hidden',
      '#value' => '1',
    );
  }
  if ($fid && $element['#file']) {
    $audiofile = file_create_url($element['#file']->uri);
    $info = pathinfo($audiofile);
    $op = $info['extension'];
    $element['filename'] = array(
      '#type' => 'markup',
      '#markup' => scm_music_player_field_get_player($audiofile, $op),
      '#weight' => -10,
    );
  }
  // Add the description field if enabled.
  if (!empty($instance['settings']['description_field']) && $item['fid']) {
    $element['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#value' => isset($item['description']) ? $item['description'] : '',
      '#type' => variable_get('file_description_type', 'textfield'),
      '#maxlength' => variable_get('file_description_length', 128),
      '#description' => t('The description may be used as the label of the link to the file.'),
    );
  }
  return $element;
}

/**
 * Implements hook_field_formatter_info().
 */
function scm_music_player_field_field_formatter_info() {
  $formatters = array(
    'scm_music_player_field_embedded' => array(
      'label' => t('scm_music_player Control with download'),
      'field types' => array(
        'file',
      ),
      'description' => t('Displays an scm_music_player player and optional download link.'),
    ),
    'scm_music_player_field_nodownload' => array(
      'label' => t('scm_music_player Control only'),
      'field types' => array(
        'file',
      ),
      'description' => t('Displays only an scm_music_player player.'),
    ),
  );
  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 */
function scm_music_player_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $elements = array();
  switch ($display['type']) {
    case 'scm_music_player_field_embedded':
    case 'scm_music_player_field_nodownload':
      global $user;
      foreach ($items as $delta => $item) {
        $audiofile = file_create_url($item['uri']);
        $info = pathinfo($audiofile);
        $op = $info['extension'];
        $options = array(
          'entity_type' => $entity_type,
          'entity' => $entity,
          'field' => $field,
          'instance' => $instance,
          'langcode' => $langcode,
          'items' => $items,
          'display' => $display,
        );
        $download_link = '';
        if ($display['type'] == 'scm_music_player_field_embedded') {
          if ($user->uid == $item['uid'] && user_access('download own audio files')) {
            $download_link .= '<div class="audio-download"><b>Download:</b>' . theme('file_link', array(
              'file' => (object) $item,
            )) . '</div>';
          }
          elseif (user_access('download all audio files')) {
            $download_link .= '<div class="audio-download"><b>Download:</b>' . theme('file_link', array(
              'file' => (object) $item,
            )) . '</div>';
          }
        }
        $elements[] = array(
          '#markup' => scm_music_player_field_get_player($audiofile, $op, $options) . $download_link,
        );
      }
      break;
  }
  return $elements;
}
