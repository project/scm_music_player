<?php
/**
 * @file
 * Implementation of scm_music_player_field.module
 */

/**
 * *Load all Field module hooks for inc files.
 */
module_load_include('inc', 'scm_music_player_field', 'scm_music_player.field');
module_load_include('inc', 'scm_music_player_field', 'scm_music_player_field.players');

/**
 * Implements hook_permission().
 */
function scm_music_player_field_permission() {
  return array(
    'download own audio files' => array(
      'title' => t('Download Own Audio Files'),
      'description' => t('Let the users download their own audio files.'),
    ),
    'download all audio files' => array(
      'title' => t('Download All Audio Files'),
      'description' => t('Let the users download any audio files.'),
    ),
  );
}

/**
 * Get the object for the suitable player for the parameter resource.
 */
function scm_music_player_field_get_player($audio_url) {
  return scm_music_player_field_scm_music_player_player($audio_url);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function scm_music_player_field_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  $instance = $form['#instance'];
  if ($instance['widget']['type'] == 'scm_music_player_field_widget' && $form['instance']['settings']['file_extensions']['#default_value'] == 'txt') {
    $form['instance']['settings']['file_extensions']['#default_value'] = 'mp3,wmv';
  }
}

/**
 * Implements hook_form_alter().
 *
 * Modify the add new field form to change the default formatter.
 */
function scm_music_player_field_form_field_ui_field_settings_form_alter(&$form, &$form_state) {
  $form['#submit'][] = 'scm_music_player_field_form_content_field_overview_submit';
}

/**
 * Submit handler to set a new field's formatter.
 */
function scm_music_player_field_form_content_field_overview_submit(&$form, &$form_state) {
  $entity_type = 'node';
  $field_name = $form_state['values']['field']['field_name'];
  $bundle = $form_state['complete form']['#bundle'];
  $instance = field_read_instance($entity_type, $field_name, $bundle);
  if ($instance['widget']['module'] == 'scm_music_player_field') {
    foreach ($instance['display'] as $display_type => $display_settings) {
      if ($instance['display'][$display_type]['type'] == 'file_default') {
        $instance['display'][$display_type]['type'] = 'scm_music_player_field_embedded';
      }
    }
    field_update_instance($instance);
  }
}

/**
 * Implements hook_field_conditional_state_settings_alter().
 *
 * Add support for Field Conditional States
 */
function scm_music_player_field_field_conditional_state_settings_alter(&$settings) {
  $settings['scm_music_player_field_widget'] = array(
    'form_elements' => array(
      0 => array(
        0,
        'upload',
      ),
    ),
    'field_data' => array(
      0,
    ),
    'reprocess_from_root' => TRUE,
    'field_states' => array(
      'enabled',
      'disabled',
      'required',
      'optional',
      'visible',
      'invisible',
    ),
    'trigger_states' => array(
      'empty',
      'filled',
    ),
    'trigger_value_widget' => '_field_conditional_state_default_trigger_value_widget',
    'trigger_value_submit' => '_field_conditional_state_default_trigger_value_submit',
  );
}
