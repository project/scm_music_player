<?php

/**
 * @file
 * Generate player field for scm music player module.
 */

/**
 * Implements player_player().
 */
function scm_music_player_field_scm_music_player_player($audio_file) {

  $tokens     = explode('/', $audio_file);
  $file_name  = $tokens[count($tokens) - 1];
  $audio_name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file_name);
  return "<div class=\"scm_music_player_item\"><span style='color:blue;font-weight:bold'>" . substr($audio_name, 0, 20) . "</span>
<button class='scm_music_player_play' onclick=\"SCM.play({title:'" . substr($audio_name, 0, 20) . "',url:'" . $audio_file . "'});\"></button>\n
<button class='scm_music_player_queue' onclick=\"SCM.queue({title:'" . substr($audio_name, 0, 20) . "',url:'" . $audio_file . "'});\"></button></div>";
}
